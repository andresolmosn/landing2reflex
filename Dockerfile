FROM node:alpine AS builder
WORKDIR app/
COPY . .
RUN npm install && npm rebuild node-sass && npm run build
FROM nginx:alpine
USER root
COPY --from=builder app/build/ /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]