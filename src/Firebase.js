import * as firebase from 'firebase';
import firestore from 'firebase/firestore'
const settings = { timestampsInSnapshots: true };

var firebaseConfig = {
    apiKey: "AIzaSyBssnBrMNLYj460xYZ4VbtH7HlxoxYcqTk",
    authDomain: "reflex-landing.firebaseapp.com",
    databaseURL: "https://reflex-landing.firebaseio.com",
    projectId: "reflex-landing",
    storageBucket: "reflex-landing.appspot.com",
    messagingSenderId: "954475432318",
    appId: "1:954475432318:web:176dde0cd01a2d8b6d4b2f"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

firebase.firestore().settings(settings);

export default firebase;