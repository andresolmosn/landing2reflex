import React from 'react';
import classes from './Footer.module.scss';

const Footer = props => {
    return (<div className={classes.Footer}>
        <p>2Reflex®</p>
    </div>)
};

export default Footer;