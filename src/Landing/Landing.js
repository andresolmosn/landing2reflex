import React, { useState } from 'react';
import classes from './Landing.module.scss'

import Circle from './Circle/Circle';
import Rectangle from './Rectangle/Rectangle';
import Triangle from './Triangle/Triangle';
import TriangleDos from './TriangleDos/TriangleDos';
import TriangleTres from './TriangleTres/TriangleTres';
import Phone from './Phone/Phone';

import Footer from './Footer/Footer';
import Nav from './Nav/Nav';
import ImagenContacto from './../imagenContacto.svg';
import firebase from '../Firebase';

const Landing = props => {
    const [phoneState, setPhoneState] = useState(false);
    const [ref] = useState(firebase.firestore().collection('Contacto'));


    const leftColumnClass = [classes.LeftColumn];
    const BackgroundClass = [classes.Background];
    const OverlayClass = [classes.Overlay];

    if (phoneState) {
        leftColumnClass.push(classes.Blur);
        BackgroundClass.push(classes.Blur);
        OverlayClass.push(classes.Active);
    }



    const nuevaSolicitudContacto = (obj) => {
        return ref.add(obj)
    }

    return (
        <div>
            <Nav />
            <div className={classes.Landing}>
                <div className={classes.Title}>2Reflex</div>
                <div className={classes.Subtitle}>Hacemos tu día más simple</div>

                <Rectangle />
                <Circle />
                <Triangle />
                <TriangleDos />
                <TriangleTres />
            </div>
            <div className={classes.Contacto} id="Contacto">
                <div className={OverlayClass.join(' ')} onClick={() => {
                    setPhoneState(!phoneState);
                }}></div>
                <div className={leftColumnClass.join(' ')}>
                    <h1>Puedes contactarnos para mas información</h1>
                    <img src={ImagenContacto} />
                    <button onClick={() => setPhoneState(!phoneState)} className={classes.Button} >Contactar</button>

                </div>
                <div style={{ width: "50%", display: 'inline-block', verticalAlign: 'middle', 'zIndex': 2 }}>
                    <Phone open={phoneState} sendHandler={nuevaSolicitudContacto} />

                </div>

                <div className={BackgroundClass.join(' ')}>
                    <div className={classes.Column}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div className={classes.Column}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div className={classes.Column}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div className={classes.Column}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div className={classes.Column}>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <Footer />
        </div>)
};

export default Landing;