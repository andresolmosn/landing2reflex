import React from 'react';
import classes from './Nav.module.scss';
import Logo from './logo.svg';
import AnchorLink from 'react-anchor-link-smooth-scroll'

const Nav = props => {
    return (<div className={classes.Nav}>
        <img src={Logo} />
        <ul>
            <li>
                <AnchorLink href='#Contacto'>Contacto</AnchorLink>
            </li>
            {/* <li>
                <a>link</a>
            </li> */}
        </ul>
    </div>);
};

export default Nav;