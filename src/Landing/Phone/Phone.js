import React, { useState } from 'react';

import classes from './Phone.module.scss';

import CircleScreen from './CircleScreen/CircleScreen';
import Input from './../../components/Input/Input';
import InputPhone from './../../components/InputPhone/InputPhone';

import Contact from './contact.svg'
import Logo from './logo.svg';
import Send from './send.svg';


const Phone = props => {
    let phoneStatus = [classes.Phone]
    const [opcion, setOpcion] = useState(false);
    const [send, setSend] = useState(false);
    const [solicitudContacto, setSolicitudContacto] = useState({
        necesidad: '',
        mailContacto: '',
        numeroContacto: ''
    });

    if (props.open) {
        phoneStatus.push(classes.First)
    }



    let render = (
        <div className={classes.Logo} style={{ color: 'white' }}>
            <img src={Logo} />
            <div className={classes.Brillo_1}></div>
            <div className={classes.Brillo_2}></div>
            <div className={classes.Brillo_3}></div>
        </div>
    )

    if (props.open && !send) {
        const classList = [classes.Form, classes.Slow];

        if (opcion) {
            classList.push(classes.Opt)
        }

        render = (<div className={classList.join(' ')}>
            <h1>Contacto</h1>
            <form>
                <Input
                    label="Nos interesa saber que necesidad tienes"
                    value={solicitudContacto.necesidad}
                    onChange={(e) => { setSolicitudContacto({ ...solicitudContacto, necesidad: e.target.value }) }}
                    validateFunction={(val) => {
                        return val.trim().length <= 0
                    }}
                />
                <Input
                    label="¿Cuál es tu correo electrónico?"
                    type="email"
                    value={solicitudContacto.mailContacto}
                    onChange={(e) => { setSolicitudContacto({ ...solicitudContacto, mailContacto: e.target.value }) }}
                    validateFunction={(val) => {
                        return !(/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(val))
                    }}
                />
                {/* <Input label="¿Cuál es tu número de teléfono?" value={solicitudContacto.numeroContacto} onChange={(e) => { setSolicitudContacto({ ...solicitudContacto, numeroContacto: e.target.value }) }} /> */}
                <InputPhone
                    label="¿Cuál es tu número de teléfono?"
                    value={solicitudContacto.numeroContacto}
                    onChange={(e) => {
                        setSolicitudContacto({ ...solicitudContacto, numeroContacto: e.target.value })
                    }}
                    validateFunction={(val) => {
                        return val.length < 12
                    }}
                />
            </form>
            <img src={Contact} />
            <button onClick={() => {
                // setOpcion(!opcion)

                if (solicitudContacto.necesidad.trim().length <= 0) {
                    console.log("Error");
                    return;
                }
                if (/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(solicitudContacto.mailContacto)) {
                    console.log(solicitudContacto.mailContacto);

                } else {
                    return;

                }

                if (solicitudContacto.numeroContacto.length < 12) {
                    return;
                }

                const nuevaSolicitudContactos = {
                    ...solicitudContacto,
                    fecha: new Date(),
                    atendido: false
                }
                // console.log(solicitudContacto);
                props.sendHandler(nuevaSolicitudContactos)
                    .then(
                        dock => {
                            setSend(true);
                            let i = classList.indexOf(classes.Slow)
                            classList.splice(i, 0)
                            classList.push(classes.hide)
                        }
                    ).catch(
                        e => { }
                    )
            }} className={classes.Button}>Solicito que me contacten</button>
        </div>)
    } else if (props.open && send) {
        const classList = [classes.Form, classes.Slow, classes.CenterImage];
        render = (<div className={classList.join(' ')}>
            <h1>Solicitud Enviada</h1>
            <img src={Send} ></img>
        </div>)

    }

    let sendStyle = null
    if (send) {
        sendStyle = { 'backgroundColor': '#36EFA0' }
    }

    return (<div className={phoneStatus.join(' ')} >
        <div className={classes.Screen}>
            <div className={classes.Camera}></div>
            <div className={classes.ScreenView} >
                <CircleScreen visible={!props.open} style={{ 'backgroundColor': '#272424' }} ></CircleScreen>
                <CircleScreen visible={props.open} style={{ 'backgroundColor': 'white' }} ></CircleScreen>
                <CircleScreen visible={props.open} style={sendStyle} ></CircleScreen>
            </div>

            <div className={classes.View}>


                {render}


            </div>
        </div>
        <div className={classes.Bottom}></div>
        <div className={classes.Border}></div>

        <div className={classes.Back}></div>

    </div>)
};

export default Phone;