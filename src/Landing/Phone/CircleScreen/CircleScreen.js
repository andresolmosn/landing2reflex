import React from 'react';
import classes from './CircleScreen.module.scss';

const CirecleScreen = props => {

    let classList = [classes.CircleScreen]
    if (props.visible) {
        classList.push(classes.Visible)
    }

    return (<div className={classList.join(' ')} style={props.style} >
        <div className={classes.Contenido}>
            {props.children}
        </div>
    </div>)
};

export default CirecleScreen;