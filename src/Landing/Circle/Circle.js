import React from 'react';
import classes from './Circle.module.scss';

const Circle = () => {
    return (<div className={classes.Circle}></div>)
};

export default Circle;