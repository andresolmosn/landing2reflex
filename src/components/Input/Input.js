import React, { useRef, useState } from 'react';
import classes from './Input.module.scss';

const Input = props => {

    let [error, setError] = useState(false);
    let input = null;
    let classList = [classes.InputComponent];
    if (error) {
        classList.push(classes.Error)
    }

    const onBlur = (e) => {
        if (props.validateFunction && props.validateFunction(e.target.value)) {
            setError(true);
        } else {
            setError(false);

        }
    }

    switch (props.type) {
        case "text":
            input = <input type="text" {...props} className={classList.join(' ')} onBlur={onBlur} />
            break;
        case "textarea":
            input = <textarea {...props} />;
            break;
        default:
            input = <input type="text" {...props} className={classList.join(' ')} onBlur={onBlur} />;
    }

    return (
        <div className={classes.Input}>
            <label>{props.label}</label>
            {input}
        </div>
    )
};

export default Input;