import React from 'react';
import Input from './../Input/Input';
import classes from './InputPhone.module.scss';

const InputPhone = props => {
    const formatPhoneNumber = (e) => {
        let phoneNumber = e.target.value;
        let phone = phoneNumber.replace(/[^0-9]/g, '');

        const match = phone.match(/^(\d{1,1})(\d{1,4})(\d{0,2})(\d{0,2})$/);
        if (match) {
            phone = `${match[1]}${match[2] ? ' ' : ''}${match[2]}${match[3] ? ' ' : ''}${match[3]}${match[4] ? ' ' : ''}${match[4]}`;
        }

        return { target: { value: phone } };
    }

    let prefix = '+56'

    return (
        <div className={classes.InputPhone}>
            <label className={classes.Prefix}>{prefix}</label>
            <Input
                label={props.label}
                onChange={e => props.onChange(formatPhoneNumber(e))}
                value={props.value}
                maxLength="12"
                style={{
                    'padding-left': '35px'
                }}
                validateFunction={props.validateFunction}
            />
        </div>
    )
};

export default InputPhone;