import React from 'react';
import classes from './Mantencion.module.scss';

import Mant from './mantencion.svg';
import Logo from './logo.svg';

const Mantencion = props => {
    return (<div className={classes.Mantencion}>
        <h1>Sitio en mantención</h1>
        <img src={Mant} className={classes.Main}></img>
        <div className={classes.Logo} >
            <img src={Logo} ></img>
            <p>2Reflex</p>
        </div>


        <div className={classes.Back}>
            <div className={classes.Column}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div className={classes.Column}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>)
};

export default Mantencion;